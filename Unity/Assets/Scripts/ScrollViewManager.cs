﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/*
 * ScrollViewManager creates the minimum GameObjects needed to display the currently visible thumbnails - as dictated by createThumbnailVOList() - as the user scrolls the view items that
 * become off screen are removed from the active list and newly visible items will take their places.
 * 
 * ItemData - represents a definition of an item describing a thumbnail, 1 for each thumbnail defined
 * ActiveInstance - represents an active item currently visible to the user
*/

public class ScrollViewManager : MonoBehaviour {

    [Header("Layout")]
    public int ItemsPerColumn = 5;
    public Vector2 Origin;
    public Vector2 Offset;

    [Header("Settings")]
    public RectTransform CanvasRectTransform;
    public RectTransform ContainerTransform;
	public GameObject prefab;

    [Header("Debug")]
    public bool ClipEarly = false;

    // Array to holding definitions of all the items
    private List<ItemData> _itemDataArray;

    // Array containing the actual game objects that will get displayed and have ItemData swapped in and out as necessary
    private List<ActiveInstance> _activeInstanceArray;

    private List<ThumbnailVO> _thumbnailVOList = new List<ThumbnailVO>();

    private void createThumbnailVOList() {
        ThumbnailVO thumbnailVO;
        for (int i = 0; i < 1000; i++){
            thumbnailVO = new ThumbnailVO();
            thumbnailVO.id = i.ToString();
            _thumbnailVOList.Add(thumbnailVO);
        }
    }

    // object representing an item in the scroll view
    class ItemData {
        private ThumbnailVO _thumbnailVO;
        private Vector3 _position;
        private int _ownerIndex = -1;
        private float _clipRadius = 0;

        public ItemData(ThumbnailVO thumbnail, Vector3 position, float clipRadius) {
            _thumbnailVO = thumbnail;
            _position = position;
            _ownerIndex = -1;
            _clipRadius = clipRadius;
        }

        public Vector3 Position {
            get {
                return _position;
            }
        }

        public Sprite GetSprite() {
            return SpriteManager.instance.GetSprite(_thumbnailVO.id);
        }

        public ThumbnailVO GetThumbnailVO() {
            return _thumbnailVO;
        }

        public bool IsVisible(Rect screenRect, bool clipEarly) {
            float y = -_position.y;
            float halfHeight = screenRect.height * 0.5f;

            float radius = _clipRadius;
            if (clipEarly) {
                radius = 0;
            }

            if (y >= screenRect.y - halfHeight - radius && y <= screenRect.y + halfHeight + radius) {
                return true;
            }

            return false;
        }

        public void SetOwnerIndex(int index) {
            // to do - ASSERT we are not owned already when setting new owner
            _ownerIndex = index;
        }

        public bool GetOwnerIndex(out int index) {
            index = _ownerIndex;
            return (_ownerIndex != -1);
        }
    };

    // object that represents a visible item on the screen
    class ActiveInstance {
        private GameObject _gameObject;
        private ItemData _Owner;  // this is the item we are representing

        public void Initialise(RectTransform container, GameObject prefab) {
            _gameObject = (GameObject)Instantiate(prefab);
            _gameObject.transform.SetParent(container, false);
            SetOwner(null);
        }

        public bool IsOwned {
            get {
                return _Owner != null;
            }
        }

        public void SetOwner(ItemData owner) {
            _Owner = owner;
            if (owner != null) {
                _gameObject.SetActive(true);
                _gameObject.transform.localPosition = owner.Position;
                _gameObject.GetComponent<Image>().sprite = owner.GetSprite();
                _gameObject.GetComponent<Thumbnail>().thumbnailVO = owner.GetThumbnailVO();
            }
            else {
                _gameObject.SetActive(false);
            }
        }
    }

    Vector2 GetSizeFromRectTransform(RectTransform rectTrans) {
        if (rectTrans != null) {
            return rectTrans.sizeDelta;
        }
        else {
            return Vector2.zero;
        }
    }

    void AddItemData(ThumbnailVO thumbnail, Vector3 position) {
        Vector2 size = GetSizeFromRectTransform((RectTransform)prefab.transform);
        _itemDataArray.Add(new ItemData(thumbnail, position, size.y * 0.5f));
    }

    // Spawn a new active instance and return its index
    int SpawnNewActiveInstance() {
        ActiveInstance item = new ActiveInstance();
        item.Initialise(ContainerTransform, prefab);
        _activeInstanceArray.Add(item);
        return _activeInstanceArray.Count - 1;
    }

    bool AllocateActiveInstance(ItemData instance, bool AllocateIfNecessary) {
        int free_index = FindFreeActiveInstanceIndex(true);
        if (free_index != -1) {
            _activeInstanceArray[free_index].SetOwner(instance);
            instance.SetOwnerIndex(free_index);
            return true;
        }

        return false;
    }

    void FreeActiveInstance(ItemData instance) {
        int index;
        if (instance.GetOwnerIndex(out index)) {
            if (index >= 0 && index < _activeInstanceArray.Count) {
                _activeInstanceArray[index].SetOwner(null);
            }
        }
        instance.SetOwnerIndex(-1);
    }

    int FindFreeActiveInstanceIndex(bool AllocateIfNecessary) {
        for (int idx = 0; idx < _activeInstanceArray.Count; idx ++) {
            ActiveInstance item = _activeInstanceArray[idx];
            if (!item.IsOwned) {
                return idx;
            }
        }

        if (AllocateIfNecessary) {
            return SpawnNewActiveInstance();
        }

        return -1;
    }

    Rect CalculateScreenRect() {
        Vector2 size = GetSizeFromRectTransform(CanvasRectTransform);
        return new Rect(ContainerTransform.localPosition.x, ContainerTransform.localPosition.y, size.x, size.y);
    }

    void Start() {
        createThumbnailVOList();

        _itemDataArray = new List<ItemData>();
        _activeInstanceArray = new List<ActiveInstance>();

        // Generate the big list of all the items
        for(int idx = 0; idx < _thumbnailVOList.Count; idx ++) {
            int x = idx % ItemsPerColumn;
            int y = idx / ItemsPerColumn;

            Vector3 offset = new Vector3(Offset.x * (float)x + Origin.x, Offset.y * (float)y + Origin.y, 0);
            AddItemData(_thumbnailVOList[idx], offset);
        }
    }

    void Update() {
        // calculate the screen rect for clipping purposes
        Rect screen_rect = CalculateScreenRect();

        // go through each instance and turn on or off as necessary
        List<ItemData> turn_on = new List<ItemData>();
        List<ItemData> turn_off = new List<ItemData>();

        foreach (ItemData instance in _itemDataArray) {
            int owning_index;
            bool is_owned = instance.GetOwnerIndex(out owning_index);

            if (instance.IsVisible(screen_rect, ClipEarly)){
                if (!is_owned) {
                    turn_on.Add(instance);
                }
            }
            else {
                if (is_owned) {
                    turn_off.Add(instance);
                }
            }
        }

        // free up resources first
        foreach (ItemData instance in turn_off) {
            FreeActiveInstance(instance);
        }

        // allocate new resources
        foreach (ItemData instance in turn_on) {
            AllocateActiveInstance(instance, true);
        }
    }
}
